package com.sergey.testtinkoff.commonservice.feign;

import com.sergey.testtinkoff.commonservice.dto.AccountDto;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

import java.util.List;
import java.util.Map;

/**
 * Account feign client.
 */
public interface AccountFeignClient {
    /**
     * Get account by id.
     *
     * @param accountId account id
     * @return account DTO
     */
    @RequestLine("GET /get/{accountId}")
    AccountDto getAccountById(@Param("accountId") long accountId);

    /**
     * Get all client's accounts DTO by id.
     *
     * @param clientId client id
     * @return accounts DTO
     */
    @RequestLine("GET /get-all-client-account/{clientId}")
    List<AccountDto> getAllByClientId(@Param("clientId") long clientId);

    /**
     * Replenish account by number.
     *
     * @param parameters account number and amount
     * @return boolean result
     */
    @RequestLine("POST /replenish/number")
    Boolean replenishAccountByNumber(@QueryMap Map<String, Object> parameters);

    /**
     * Withdraw account by number.
     *
     * @param parameters account number and amount
     * @return boolean result
     */
    @RequestLine("POST /withdraw/number")
    Boolean withdrawAccountByNumber(@QueryMap Map<String, Object> parameters);
}
