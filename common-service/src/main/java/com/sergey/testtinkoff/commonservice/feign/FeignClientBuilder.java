package com.sergey.testtinkoff.commonservice.feign;

import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Feign client builder for customer and account services.
 */
@Component
public class FeignClientBuilder {
    @Value("${customer.url}")
    private String CUSTOMER_URL;
    @Value("${account.url}")
    private String ACCOUNT_URL;

    private CustomerFeignClient customerClient;
    private AccountFeignClient accountClient;

    private <T> T createClient(Class<T> type, String uri) {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(type))
                .logLevel(Logger.Level.FULL)
                .target(type, uri);
    }

    /**
     * Getter for customer feign client.
     *
     * @return customer feign client
     */
    public CustomerFeignClient getCustomerClient() {
        return customerClient;
    }

    /**
     * Getter for account feign client.
     *
     * @return account feign client
     */
    public AccountFeignClient getAccountClient() {
        return accountClient;
    }

    @PostConstruct
    private void init(){
        customerClient = createClient(CustomerFeignClient.class, CUSTOMER_URL);
        accountClient = createClient(AccountFeignClient.class, ACCOUNT_URL);
    }
}
