package com.sergey.testtinkoff.commonservice.service.commands;

import com.sergey.testtinkoff.commonservice.feign.AccountFeignClient;
import com.sergey.testtinkoff.commonservice.feign.FeignClientBuilder;

import java.util.Map;

/**
 * Withdraw command.
 */
public class Withdraw extends Command {

    public Withdraw(Map<String, Object> parameters, FeignClientBuilder feignClientBuilder) {
        super(parameters, feignClientBuilder);
    }

    /**
     * Execute command.
     *
     * @return result of execution command
     */
    @Override
    public boolean execute() {
        AccountFeignClient accountFeignClient = feignClientBuilder.getAccountClient();
        return accountFeignClient.withdrawAccountByNumber(parameters);
    }

    /**
     * Undo command.
     */
    @Override
    public void undo() {
        feignClientBuilder.getAccountClient().replenishAccountByNumber(parameters);
    }
}
