package com.sergey.testtinkoff.commonservice.service.commands;

import java.util.LinkedList;

/**
 * History of commands.
 */
public class CommandHistory {
    private LinkedList<Command> history = new LinkedList<>();

    /**
     * Push new command to history.
     *
     * @param c command
     */
    public void push(Command c) {
        history.push(c);
    }

    /**
     * Get last command from history.
     *
     * @return command
     */
    public Command pop() {
        return history.pop();
    }

    /**
     * Check history of command, empty or not.
     *
     * @return empty or not
     */
    public boolean isEmpty() { return history.isEmpty(); }
}
