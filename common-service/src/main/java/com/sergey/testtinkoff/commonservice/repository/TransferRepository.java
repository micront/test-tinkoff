package com.sergey.testtinkoff.commonservice.repository;

import com.sergey.testtinkoff.commonservice.domian.Transfer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransferRepository extends CrudRepository<Transfer, Long> {
}
