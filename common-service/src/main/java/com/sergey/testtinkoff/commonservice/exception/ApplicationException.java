package com.sergey.testtinkoff.commonservice.exception;

/**
 * Custom exception, for every checked error in application.
 */
public class ApplicationException extends Exception {
    /**
     * Creates exception.
     *
     * @param message of exception
     */
    public ApplicationException(String message) {
        super(message);
    }
}
