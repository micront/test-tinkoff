package com.sergey.testtinkoff.commonservice.dto;

/**
 * DTO for transfer.
 */
public class TransferForm {
    private long accountNumberFrom;
    private long accountNumberTo;
    private float amount;

    public TransferForm() {
    }

    /**
     * Getter for accountNumberFrom.
     *
     * @return accountNumberFrom
     */
    public long getAccountNumberFrom() {
        return accountNumberFrom;
    }

    /**
     * Setter for accountNumberFrom.
     *
     * @param accountNumberFrom accountNumberFrom
     */
    public void setAccountNumberFrom(long accountNumberFrom) {
        this.accountNumberFrom = accountNumberFrom;
    }

    /**
     * Getter for accountNumberTo.
     *
     * @return accountNumberTo
     */
    public long getAccountNumberTo() {
        return accountNumberTo;
    }

    /**
     * Setter for accountNumberTo.
     *
     * @param accountNumberTo accountNumberTo
     */
    public void setAccountNumberTo(long accountNumberTo) {
        this.accountNumberTo = accountNumberTo;
    }

    /**
     * Getter for amount.
     *
     * @return amount
     */
    public float getAmount() {
        return amount;
    }

    /**
     * Setter for amount.
     *
     * @param amount amount
     */
    public void setAmount(float amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "TransferForm{" +
                "accountNumberFrom=" + accountNumberFrom +
                ", accountNumberTo=" + accountNumberTo +
                ", amount=" + amount +
                '}';
    }
}
