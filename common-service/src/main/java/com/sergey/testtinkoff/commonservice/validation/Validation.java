package com.sergey.testtinkoff.commonservice.validation;

import com.sergey.testtinkoff.commonservice.dto.TransferForm;
import com.sergey.testtinkoff.commonservice.exception.ApplicationRuntimeException;

/**
 * Util class for validation.
 */
public class Validation {
    private static final String NUMBER_CANT_BE_NEGATIVE = "Account number can not be negative";
    private static final String INCORRECT_AMOUNT = "Incorrect transfer amount";
    private static final String CUSTOMER_ID_CANT_BE_NEGATIVE = "Customer id can not be negative";

    /**
     * Validate customer id.
     *
     * @param customerId customer id
     */
    public static void validateCustomerId(long customerId) {
        assertNotNegative(customerId, CUSTOMER_ID_CANT_BE_NEGATIVE);
    }

    /**
     * Validate transferForm.
     *
     * @param transferForm transferForm
     */
    public static void validateTransferForm(TransferForm transferForm) {
        validateNumber(transferForm.getAccountNumberTo());
        validateNumber(transferForm.getAccountNumberFrom());
        validateAmount(transferForm.getAmount());

    }

    /**
     * Validate account number.
     *
     * @param number account number
     */
    public static void validateNumber(long number) {
        assertNotNegative(number, NUMBER_CANT_BE_NEGATIVE);
    }

    /**
     * Validate that customer id doesn't negative or 0.
     *
     * @param customerId customer id
     * @param message    error message
     */
    public static void assertNotNegative(long customerId, String message) {
        if (customerId < 0) {
            throw new ApplicationRuntimeException(message);
        }
    }

    /**
     * Validate transfer amount.
     *
     * @param amount customer age
     */
    public static void validateAmount(float amount) {
        if (amount < 0) {
            throw new ApplicationRuntimeException(INCORRECT_AMOUNT);
        }
    }
}
