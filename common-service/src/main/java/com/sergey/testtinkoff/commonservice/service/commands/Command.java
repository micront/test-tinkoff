package com.sergey.testtinkoff.commonservice.service.commands;

import com.sergey.testtinkoff.commonservice.feign.FeignClientBuilder;

import java.util.Map;

/**
 * Realization Command pattern.
 */
public abstract class Command {

    protected Map<String, Object> parameters;
    protected FeignClientBuilder feignClientBuilder;

    Command(Map<String, Object> parameters, FeignClientBuilder feignClientBuilder){
        this.parameters = parameters;
        this.feignClientBuilder = feignClientBuilder;
    }

    /**
     * Undo command.
     */
    public abstract void undo();

    /**
     * Execute command.
     *
     * @return result of execution command
     */
    public abstract boolean execute();
}
