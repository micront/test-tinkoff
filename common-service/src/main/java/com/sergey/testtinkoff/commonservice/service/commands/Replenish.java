package com.sergey.testtinkoff.commonservice.service.commands;

import com.sergey.testtinkoff.commonservice.feign.AccountFeignClient;
import com.sergey.testtinkoff.commonservice.feign.FeignClientBuilder;

import java.util.Map;

/**
 * Replenish command.
 */
public class Replenish extends Command {

    public Replenish(Map<String, Object> parameters, FeignClientBuilder feignClientBuilder) {
        super(parameters, feignClientBuilder);
    }

    /**
     * Execute command.
     *
     * @return result of execution command
     */
    @Override
    public boolean execute() {
        AccountFeignClient accountFeignClient = feignClientBuilder.getAccountClient();
        return accountFeignClient.replenishAccountByNumber(parameters);
    }

    /**
     * Undo command.
     */
    @Override
    public void undo() {
        feignClientBuilder.getAccountClient().withdrawAccountByNumber(parameters);
    }




}
