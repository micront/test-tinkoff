package com.sergey.testtinkoff.commonservice.dto;

import java.util.List;

/**
 * DTO for customers with accounts.
 */
public class CustomerWithAccounts {
    private long id;
    private String firstName;
    private String lastName;
    private int age;
    private List<AccountDto> accounts;

    public CustomerWithAccounts() {
    }

    public CustomerWithAccounts(CustomerDto customerDto, List<AccountDto> accounts) {
        id = customerDto.getId();
        firstName = customerDto.getFirstName();
        lastName = customerDto.getLastName();
        age = customerDto.getAge();
        this.accounts = accounts;
    }

    /**
     * Getter for id.
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter for id.
     *
     * @param id id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Getter for first name.
     *
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter for first name.
     *
     * @param firstName first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter for last name.
     *
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter for last name.
     *
     * @param lastName last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter for age.
     *
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * Setter for age.
     *
     * @param age age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Getter for accounts.
     *
     * @return accounts DTO
     */
    public List<AccountDto> getAccounts() {
        return accounts;
    }

    /**
     * Setter for accounts.
     *
     * @param accounts accounts DTO
     */
    public void setAccounts(List<AccountDto> accounts) {
        this.accounts = accounts;
    }
}
