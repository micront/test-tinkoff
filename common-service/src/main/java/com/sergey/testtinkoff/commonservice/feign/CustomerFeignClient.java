package com.sergey.testtinkoff.commonservice.feign;

import com.sergey.testtinkoff.commonservice.dto.CustomerDto;
import feign.Param;
import feign.RequestLine;

/**
 * Customer feign client.
 */
public interface CustomerFeignClient {

    /**
     * Get customer by id.
     *
     * @param customerId customer id
     * @return customer DTO
     */
    @RequestLine("GET /get/{customerId}")
    CustomerDto getCustomerById(@Param("customerId") long customerId);
}
