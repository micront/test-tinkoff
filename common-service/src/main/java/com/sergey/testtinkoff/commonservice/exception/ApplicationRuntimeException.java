package com.sergey.testtinkoff.commonservice.exception;

/**
 * Custom runtime exception, for every runtime error in application.
 */
public class ApplicationRuntimeException extends RuntimeException {
    /**
     * Creates default exception.
     */
    public ApplicationRuntimeException() {
    }

    /**
     * Creates exception with message.
     *
     * @param message of exception
     */
    public ApplicationRuntimeException(String message) {
        super(message);
    }

    /**
     * Creates exception with message and cause.
     *
     * @param message of exception
     * @param cause of exception
     */
    public ApplicationRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }


    /**
     * Creates exception with cause.
     *
     * @param cause of exception
     */
    public ApplicationRuntimeException(Throwable cause) {
        super(cause);
    }
}
