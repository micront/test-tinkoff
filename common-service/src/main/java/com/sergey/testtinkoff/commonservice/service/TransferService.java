package com.sergey.testtinkoff.commonservice.service;

import com.sergey.testtinkoff.commonservice.dto.TransferForm;

/**
 * Transfer service interface.
 */
public interface TransferService {
    boolean transfer(TransferForm... transferForm);
}
