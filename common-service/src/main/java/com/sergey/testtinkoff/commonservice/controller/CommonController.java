package com.sergey.testtinkoff.commonservice.controller;

import com.sergey.testtinkoff.commonservice.dto.CustomerWithAccounts;
import com.sergey.testtinkoff.commonservice.dto.TransferForm;
import com.sergey.testtinkoff.commonservice.feign.FeignClientBuilder;
import com.sergey.testtinkoff.commonservice.dto.AccountDto;
import com.sergey.testtinkoff.commonservice.feign.AccountFeignClient;
import com.sergey.testtinkoff.commonservice.dto.CustomerDto;
import com.sergey.testtinkoff.commonservice.feign.CustomerFeignClient;
import com.sergey.testtinkoff.commonservice.service.TransferService;
import com.sergey.testtinkoff.commonservice.validation.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Common controller.
 */
@RestController
@RequestMapping("/common")
public class CommonController {

    private AccountFeignClient accountClient;
    private CustomerFeignClient customerClient;
    private TransferService transferService;


    @Autowired
    CommonController(FeignClientBuilder feignClientBuilder, TransferService transferService){
        accountClient = feignClientBuilder.getAccountClient();
        customerClient = feignClientBuilder.getCustomerClient();
        this.transferService = transferService;
    }

    /**
     * Get customer information with accounts information by customer id.
     *
     * @param customerId customer id
     * @return customer with accounts
     */
    @RequestMapping(value = "/get-client/{customerId}", method = RequestMethod.GET)
    public CustomerWithAccounts getAccount(@PathVariable("customerId") long customerId) {
        Validation.validateCustomerId(customerId);
        CustomerDto customerDto = customerClient.getCustomerById(customerId);
        List<AccountDto> accounts = accountClient.getAllByClientId(customerId);
        return new CustomerWithAccounts(customerDto, accounts);
    }

    /**
     * Execute money transfer.
     *
     * @param transferForm transfer information
     * @return transfer result
     */
    @RequestMapping(value = "/money-transfer", method = RequestMethod.POST)
    public ResponseEntity<Boolean> moneyTransfer(@RequestBody TransferForm transferForm) {
        Validation.validateTransferForm(transferForm);
        boolean result = transferService.transfer(transferForm);

        return ResponseEntity.ok(result);
    }

}
