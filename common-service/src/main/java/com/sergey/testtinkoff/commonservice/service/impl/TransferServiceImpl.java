package com.sergey.testtinkoff.commonservice.service.impl;

import com.sergey.testtinkoff.commonservice.domian.Transfer;
import com.sergey.testtinkoff.commonservice.dto.TransferForm;
import com.sergey.testtinkoff.commonservice.exception.ApplicationRuntimeException;
import com.sergey.testtinkoff.commonservice.feign.FeignClientBuilder;
import com.sergey.testtinkoff.commonservice.mapper.EntityToDtoMapper;
import com.sergey.testtinkoff.commonservice.repository.TransferRepository;
import com.sergey.testtinkoff.commonservice.service.TransferService;
import com.sergey.testtinkoff.commonservice.service.commands.Command;
import com.sergey.testtinkoff.commonservice.service.commands.CommandHistory;
import com.sergey.testtinkoff.commonservice.service.commands.Replenish;
import com.sergey.testtinkoff.commonservice.service.commands.Withdraw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Implementation transfer service.
 */
@Service
public class TransferServiceImpl implements TransferService {

    private FeignClientBuilder feignClientBuilder;
    private TransferRepository repository;

    @Autowired
    TransferServiceImpl(FeignClientBuilder feignClientBuilder, TransferRepository repository){
        this.repository = repository;
        this.feignClientBuilder = feignClientBuilder;
    }

    /**
     * Execution of translations, if errors occur, changes are rolled back.
     *
     * @param transferForms array transfers
     * @return result execution
     */
    @Override
    public boolean transfer(TransferForm... transferForms) {
        CommandHistory commandHistory = new CommandHistory();
        try {
            for (TransferForm transfer : transferForms) {
                Map<String, Object> parametersWithdraw = new LinkedHashMap<>();
                parametersWithdraw.put("accountNumber", transfer.getAccountNumberFrom());
                parametersWithdraw.put("amount", transfer.getAmount());

                Map<String, Object> parametersReplenish = new LinkedHashMap<>();
                parametersReplenish.put("accountNumber", transfer.getAccountNumberTo());
                parametersReplenish.put("amount", transfer.getAmount());
                Command withdraw = new Withdraw(parametersWithdraw, feignClientBuilder);
                boolean withdrawResult = withdraw.execute();
                if (withdrawResult) {
                    commandHistory.push(withdraw);
                    Command replenish = new Replenish(parametersReplenish, feignClientBuilder);
                    boolean replenishResult = replenish.execute();
                    if (replenishResult) {
                        commandHistory.push(replenish);
                    } else {
                        cancel(commandHistory);
                        break;
                    }
                } else {
                    cancel(commandHistory);
                    break;
                }
            }
        } catch (Exception e){
            cancel(commandHistory);
            throw new ApplicationRuntimeException("Canceled");
        }

        if (!commandHistory.isEmpty()) {
            repository.saveAll(transformTransfers(transferForms));
            return true;
        }
        return false;
    }


    private void cancel(CommandHistory commandHistory) {
        while (!commandHistory.isEmpty()) {
            commandHistory.pop().undo();
        }
    }

    private List<Transfer> transformTransfers(TransferForm... transferForms) {
        return Arrays.stream(transferForms)
                .map(EntityToDtoMapper::dtoToEntity)
                .collect(Collectors.toList());
    }
}
