package com.sergey.testtinkoff.commonservice.mapper;

import com.sergey.testtinkoff.commonservice.domian.Transfer;
import com.sergey.testtinkoff.commonservice.dto.TransferForm;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;

@Component
public class EntityToDtoMapper {
    public static Transfer dtoToEntity(TransferForm transferForm) {
        Transfer transfer = new Transfer();
        transfer.setAccountNumberFrom(transferForm.getAccountNumberFrom());
        transfer.setAccountNumberTo(transferForm.getAccountNumberTo());
        transfer.setAmount(transferForm.getAmount());
        transfer.setDate(new Timestamp(Instant.now().toEpochMilli()));
        return transfer;
    }

}
