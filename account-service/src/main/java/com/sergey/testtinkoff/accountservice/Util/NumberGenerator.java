package com.sergey.testtinkoff.accountservice.Util;

import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Account numbers generator.
 */
@Component
public class NumberGenerator {
    private static AtomicLong COUNT = new AtomicLong(400);

    /**
     * Generate account number.
     *
     * @return account number
     */
    public static long generate(){
        return COUNT.incrementAndGet();
    }
}
