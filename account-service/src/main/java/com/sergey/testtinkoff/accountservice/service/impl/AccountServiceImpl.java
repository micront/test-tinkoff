package com.sergey.testtinkoff.accountservice.service.impl;

import com.sergey.testtinkoff.accountservice.Util.NumberGenerator;
import com.sergey.testtinkoff.accountservice.dto.AccountDto;
import com.sergey.testtinkoff.accountservice.domain.Account;
import com.sergey.testtinkoff.accountservice.exception.ApplicationRuntimeException;
import com.sergey.testtinkoff.accountservice.mapper.EntityToDtoMapper;
import com.sergey.testtinkoff.accountservice.repository.AccountRepository;
import com.sergey.testtinkoff.accountservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

/**
 * Implementation account service.
 */
@Transactional
@Service
public class AccountServiceImpl implements AccountService {
    private static final String CANT_FIND_ACCOUNT = "Can't find account in database";
    private AccountRepository accountRepository;
    private EntityToDtoMapper mapper;

    /**
     * Constructor account service.
     *
     * @param accountRepository account repository
     * @param mapper            entity to DTO mapper
     */
    @Autowired
    AccountServiceImpl(AccountRepository accountRepository, EntityToDtoMapper mapper) {
        this.accountRepository = accountRepository;
        this.mapper = mapper;
    }

    /**
     * Get account by id.
     *
     * @param id account id
     * @return account DTO
     */
    @Override
    public AccountDto getAccountById(long id) {
        Account account = accountRepository
                .findById(id)
                .orElseThrow(() -> new ApplicationRuntimeException(CANT_FIND_ACCOUNT));
        return mapper.entityToDto(account);
    }

    /**
     * Create account for clients.
     *
     * @param clientId client id
     * @return account number
     */
    @Override
    public long createAccount(long clientId) {
        long number = NumberGenerator.generate();
        Account account = new Account(number,
                0F,
                clientId);
        accountRepository.save(account);
        return number;
    }

    /**
     * Delete account by id.
     *
     * @param id account id
     * @return delete result
     */
    @Override
    public boolean deleteAccountById(long id) {
        accountRepository.deleteById(id);
        return !accountRepository.findById(id).isPresent();
    }

    /**
     * Replenish account by id.
     *
     * @param accountId account id
     * @param amount    amount
     * @return replenish result
     */
    @Override
    public boolean replenishAccountById(long accountId, float amount) {
        Account account = accountRepository
                .findById(accountId)
                .orElseThrow(() -> new ApplicationRuntimeException(CANT_FIND_ACCOUNT));
        account.setBalance(account.getBalance() + amount);
        accountRepository.save(account);
        return true;
    }

    /**
     * Replenish account by number.
     *
     * @param accountNumber account number
     * @param amount        amount
     * @return replenish result
     */
    @Override
    public boolean replenishAccountByNumber(long accountNumber, float amount) {
        Account account = accountRepository.findByNumber(accountNumber);
        account.setBalance(account.getBalance() + amount);
        accountRepository.save(account);
        return true;
    }

    /**
     * Withdraw account by number.
     *
     * @param accountNumber account number
     * @param amount        amount
     * @return withdraw result
     */
    @Override
    public boolean withdrawAccountByNumber(long accountNumber, float amount) {
        Account account = accountRepository.findByNumber(accountNumber);
        if (account.getBalance() >= amount) {
            account.setBalance(account.getBalance() - amount);
            accountRepository.save(account);
            return true;
        } else
            return false;
    }

    /**
     * Withdraw account by id.
     *
     * @param accountId account id
     * @param amount    amount
     * @return withdraw result
     */
    @Override
    public boolean withdrawAccountById(long accountId, float amount) {
        Account account = accountRepository
                .findById(accountId)
                .orElseThrow(() -> new ApplicationRuntimeException(CANT_FIND_ACCOUNT));
        if (account.getBalance() >= amount) {
            account.setBalance(account.getBalance() - amount);
            accountRepository.save(account);
            return true;
        } else
            return false;
    }

    /**
     * Gett all accounts by holder id.
     *
     * @param clientId client id
     * @return accounts DTO
     */
    @Override
    public List<AccountDto> getAllAccountByClientId(long clientId) {
        List<Account> allByHolderId = accountRepository.findAllByHolderId(clientId);
        List<AccountDto> accountDtos = new LinkedList<>();
        for (Account account : allByHolderId) {
            accountDtos.add(mapper.entityToDto(account));
        }
        return accountDtos;
    }
}
