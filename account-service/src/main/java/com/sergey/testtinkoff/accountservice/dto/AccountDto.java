package com.sergey.testtinkoff.accountservice.dto;

/**
 * DTO for account entity.
 */
public class AccountDto {

    private long id;
    private long number;
    private float balance;
    private long holderId;

    /**
     * Constructor account DTO.
     */
    public AccountDto() {
    }

    /**
     * Constructor account DTO.
     *
     * @param id id
     * @param number number
     * @param balance balance
     * @param holderId holder id
     */
    public AccountDto(long id, long number, float balance, long holderId) {
        this.id = id;
        this.number = number;
        this.balance = balance;
        this.holderId = holderId;
    }

    /**
     * Getter for id.
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter for id.
     *
     * @param id id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Getter for number.
     *
     * @return number
     */
    public long getNumber() {
        return number;
    }

    /**
     * Setter for number.
     *
     * @param number number
     */
    public void setNumber(long number) {
        this.number = number;
    }

    /**
     * Getter for balance.
     *
     * @return balance
     */
    public float getBalance() {
        return balance;
    }

    /**
     * Setter for balance.
     *
     * @param balance balance
     */
    public void setBalance(float balance) {
        this.balance = balance;
    }

    /**
     * Getter for holderId.
     *
     * @return holderId
     */
    public long getHolderId() {
        return holderId;
    }

    /**
     * Setter for holderId.
     *
     * @param holderId holderId
     */
    public void setHolderId(long holderId) {
        this.holderId = holderId;
    }

}
