package com.sergey.testtinkoff.accountservice.repository;

import com.sergey.testtinkoff.accountservice.domain.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for accounts.
 */
@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

    /**
     * Find account by number.
     *
     * @param number number
     * @return account
     */
    Account findByNumber(long number);

    /**
     * Find all accounts by holder id.
     *
     * @param clientId holder id
     * @return accounts
     */
    List<Account> findAllByHolderId(long clientId);
}

