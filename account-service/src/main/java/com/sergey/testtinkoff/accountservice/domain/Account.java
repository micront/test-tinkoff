package com.sergey.testtinkoff.accountservice.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Domain entity for account.
 */
@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    private long number;

    private float balance;

    private long holderId;

    /**
     * Constructor account entity.
     */
    public Account() {
    }

    /**
     * Constructor account entity.
     *
     * @param number number
     * @param balance balance
     * @param holderId holder id
     */
    public Account(long number, float balance, long holderId) {
        this.number = number;
        this.balance = balance;
        this.holderId = holderId;
    }

    /**
     * Getter for id.
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter for id.
     *
     * @param id id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Getter for number.
     *
     * @return number
     */
    public long getNumber() {
        return number;
    }

    /**
     * Setter for number.
     *
     * @param number number
     */
    public void setNumber(long number) {
        this.number = number;
    }

    /**
     * Getter for balance.
     *
     * @return balance
     */
    public float getBalance() {
        return balance;
    }

    /**
     * Setter for balance.
     *
     * @param balance balance
     */
    public void setBalance(float balance) {
        this.balance = balance;
    }

    /**
     * Getter for holderId.
     *
     * @return holderId
     */
    public long getHolderId() {
        return holderId;
    }

    /**
     * Setter for holderId.
     *
     * @param holderId holderId
     */
    public void setHolderId(long holderId) {
        this.holderId = holderId;
    }
}
