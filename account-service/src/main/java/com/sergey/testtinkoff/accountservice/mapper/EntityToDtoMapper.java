package com.sergey.testtinkoff.accountservice.mapper;

import com.sergey.testtinkoff.accountservice.domain.Account;
import com.sergey.testtinkoff.accountservice.dto.AccountDto;
import org.springframework.stereotype.Component;

/**
 * Class for mapping DTO to entity and back.
 */
@Component
public class EntityToDtoMapper {
    /**
     * Mapping entity to DTO.
     *
     * @param account account
     * @return account DTO
     */
    public AccountDto entityToDto(Account account) {
        AccountDto accountDto = new AccountDto();
        accountDto.setId(account.getId());
        accountDto.setNumber(account.getNumber());
        accountDto.setBalance(account.getBalance());
        accountDto.setHolderId(account.getHolderId());
        return accountDto;
    }

    /**
     * Mapping DTO to entity.
     *
     * @param accountDto account DTO
     * @return account
     */
    public Account dtoToEntity(AccountDto accountDto) {
        Account account = new Account();
        account.setId(accountDto.getId());
        account.setNumber(accountDto.getNumber());
        account.setBalance(accountDto.getBalance());
        account.setHolderId(accountDto.getHolderId());
        return account;
    }

}
