package com.sergey.testtinkoff.accountservice.service;

import com.sergey.testtinkoff.accountservice.dto.AccountDto;

import java.util.List;

/**
 * Service to manage accounts.
 */
public interface AccountService {
    /**
     * Get account by id.
     *
     * @param id account id
     * @return account DTO
     */
    AccountDto getAccountById(long id);

    /**
     * Create account for client by client id.
     *
     * @param clientId client id
     * @return account number
     */
    long createAccount(long clientId);

    /**
     * Delete account by id.
     *
     * @param id account id
     * @return delete result
     */
    boolean deleteAccountById(long id);

    /**
     * Replenish account by account id.
     *
     * @param accountId account id
     * @param amount amount
     * @return replenish result
     */
    boolean replenishAccountById(long accountId, float amount);

    /**
     * Replenish account by account number.
     *
     * @param accountNumber account number
     * @param amount amount
     * @return replenish result
     */
    boolean replenishAccountByNumber(long accountNumber, float amount);

    /**
     * Withdraw account by account number.
     *
     * @param accountNumber account number
     * @param amount amount
     * @return replenish result
     */
    boolean withdrawAccountByNumber(long accountNumber, float amount);

    /**
     * Withdraw account by account id.
     *
     * @param accountId account id
     * @param amount amount
     * @return replenish result
     */
    boolean withdrawAccountById(long accountId, float amount);

    /**
     * Get all accounts by client id.
     *
     * @param clientId client id
     * @return accounts DTO
     */
    List<AccountDto> getAllAccountByClientId(long clientId);
}
