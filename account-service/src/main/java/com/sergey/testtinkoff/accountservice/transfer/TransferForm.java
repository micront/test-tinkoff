package com.sergey.testtinkoff.accountservice.transfer;

/**
 * DTO for change account amount.
 */
public class TransferForm {
    private long accountNumber;
    private float amount;
    private long accountId;

    /**
     * Getter account number.
     *
     * @return number
     */
    public long getAccountNumber() {
        return accountNumber;
    }

    /**
     * Setter account number.
     *
     * @param accountNumber account number
     */
    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * Getter for amount.
     *
     * @return amount
     */
    public float getAmount() {
        return amount;
    }

    /**
     * Setter for amount.
     *
     * @param amount amount
     */
    public void setAmount(float amount) {
        this.amount = amount;
    }

    /**
     * Getter for account id.
     *
     * @return account id
     */
    public long getAccountId() {
        return accountId;
    }

    /**
     * Setter for account id.
     *
     * @param accountId account id
     */
    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }
}
