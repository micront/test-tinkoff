package com.sergey.testtinkoff.accountservice.validation;

import com.sergey.testtinkoff.accountservice.exception.ApplicationRuntimeException;

/**
 * Util class for validation.
 */
public class Validation {
    private static final String ACCOUNT_ID_CANT_BE_NEGATIVE = "Account id cannot be negative";
    private static final String USER_ID_CANT_BE_NEGATIVE = "User id cannot be negative";
    private static final String ACCOUNT_NUMBER_CANT_BE_NEGATIVE = "User id cannot be negative";
    private static final String INCORRECT_AMOUNT = "Incorrect transfer amount";

    /**
     * Validate account id.
     *
     * @param accountId account id
     */
    public static void validateAccountId(long accountId) {
        assertNotNegative(accountId, ACCOUNT_ID_CANT_BE_NEGATIVE);
    }

    /**
     * Validate customer id.
     *
     * @param customerId customer id
     */
    public static void validateClientId(long customerId) {
        assertNotNegative(customerId, USER_ID_CANT_BE_NEGATIVE);
    }

    /**
     * Validate account number.
     *
     * @param accountNumber account number
     */
    public static void validateAccountNumber(long accountNumber) {
        assertNotNegative(accountNumber, ACCOUNT_NUMBER_CANT_BE_NEGATIVE);
    }
    /**
     * Validate that customer id doesn't negative or 0.
     *
     * @param customerId customer id
     * @param message    error message
     */
    public static void assertNotNegative(long customerId, String message) {
        if (customerId < 0) {
            throw new ApplicationRuntimeException(message);
        }
    }

    /**
     * Validate transfer amount.
     *
     * @param amount customer age
     */
    public static void validateAmount(float amount) {
        if (amount < 0) {
            throw new ApplicationRuntimeException(INCORRECT_AMOUNT);
        }
    }

}
