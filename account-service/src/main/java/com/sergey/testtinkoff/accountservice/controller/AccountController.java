package com.sergey.testtinkoff.accountservice.controller;

import com.sergey.testtinkoff.accountservice.dto.AccountDto;
import com.sergey.testtinkoff.accountservice.service.AccountService;
import com.sergey.testtinkoff.accountservice.validation.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * Account controller.
 */
@RestController
@RequestMapping("/account")
public class AccountController {

    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * Get account by account id.
     *
     * @param accountId accountId
     * @return accountDto
     */
    @RequestMapping(value = "/get/{accountId}", method = RequestMethod.GET)
    public AccountDto getAccount(@PathVariable("accountId") long accountId) {
        Validation.validateAccountId(accountId);
        return accountService.getAccountById(accountId);
    }

    /**
     * Create account by client id.
     *
     * @param clientId client id
     * @return number account
     */
    @RequestMapping(value = "/create/{clientId}", method = RequestMethod.POST)
    public String createAccount(@PathVariable("clientId") long clientId) {
        Validation.validateClientId(clientId);
        long numberAccount = accountService.createAccount(clientId);
        return "Number account: " + numberAccount;
    }

    /**
     * Delete account by account id.
     *
     * @param accountId client id
     * @return result
     */
    @RequestMapping(value = "/delete/{accountId}", method = RequestMethod.POST)
    public ResponseEntity<Boolean> deleteAccount(@PathVariable("accountId") long accountId) {
        Validation.validateAccountId(accountId);
        boolean result = accountService.deleteAccountById(accountId);
        return ResponseEntity.ok(result);
    }

    /**
     * Replenish account by id
     *
     * @param accountIdReq account id
     * @param amountReq amount
     * @return replenish result
     */
    @RequestMapping(value = "/replenish/id", method = RequestMethod.POST)
    public ResponseEntity<Boolean> replenishAccountById(@RequestParam("accountId") String accountIdReq,
                                                        @RequestParam("amount") String amountReq) {
        long accountId = Long.valueOf(accountIdReq);
        float amount = Float.valueOf(amountReq);
        Validation.validateAccountId(accountId);
        Validation.validateAmount(amount);
        boolean result = accountService.replenishAccountById(accountId, amount);
        return ResponseEntity.ok(result);
    }

    /**
     * Replenish account by number.
     *
     * @param accountNumberReq account number
     * @param amountReq amount
     * @return replenish result
     */
    @RequestMapping(value = "/replenish/number", method = RequestMethod.POST)
    public ResponseEntity<Boolean> replenishAccountByNumber(@RequestParam("accountNumber") String accountNumberReq,
                                                            @RequestParam("amount") String amountReq) {
        long accountNumber = Long.valueOf(accountNumberReq);
        float amount = Float.valueOf(amountReq);
        Validation.validateAccountNumber(accountNumber);
        Validation.validateAmount(amount);
        boolean result = accountService.replenishAccountByNumber(accountNumber, amount);
        return ResponseEntity.ok(result);
    }

    /**
     * Withdraw account by id
     *
     * @param accountIdReq account id
     * @param amountReq amount
     * @return withdraw result
     */
    @RequestMapping(value = "/withdraw/id", method = RequestMethod.POST)
    public ResponseEntity<Boolean> withdrawAccountById(@RequestParam("accountId") String accountIdReq,
                                                       @RequestParam("amount") String amountReq) {
        long accountId = Long.valueOf(accountIdReq);
        float amount = Float.valueOf(amountReq);
        Validation.validateAccountId(accountId);
        Validation.validateAmount(amount);
        boolean result = accountService.withdrawAccountById(accountId, amount);
        return ResponseEntity.ok(result);
    }

    /**
     * Withdraw account by number.
     *
     * @param accountNumberReq account number
     * @param amountReq amount
     * @return withdraw result
     */
    @RequestMapping(value = "/withdraw/number", method = RequestMethod.POST)
    public ResponseEntity<Boolean> withdrawAccountByNumber(@RequestParam("accountNumber") String accountNumberReq,
                                                           @RequestParam("amount") String amountReq) {
        long accountNumber = Long.valueOf(accountNumberReq);
        float amount = Float.valueOf(amountReq);
        Validation.validateAccountNumber(accountNumber);
        Validation.validateAmount(amount);
        boolean result = accountService.withdrawAccountByNumber(accountNumber, amount);
        return ResponseEntity.ok(result);
    }

    /**
     * Get all client's accounts by client id.
     *
     * @param clientId client id
     * @return accounts DTO
     */
    @RequestMapping(value = "/get-all-client-account/{clientId}", method = RequestMethod.GET)
    public List<AccountDto> withdrawAccountByNumber(@PathVariable("clientId") long clientId) {
        Validation.validateAccountId(clientId);
        return accountService.getAllAccountByClientId(clientId);
    }

}
