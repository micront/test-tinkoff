package com.sergey.testtinkoff.customerservice.service.impl;

import com.sergey.testtinkoff.customerservice.domain.Customer;
import com.sergey.testtinkoff.customerservice.dto.CustomerDto;
import com.sergey.testtinkoff.customerservice.exception.ApplicationRuntimeException;
import com.sergey.testtinkoff.customerservice.mapper.EntityToDtoMapper;
import com.sergey.testtinkoff.customerservice.repository.CustomerRepository;
import com.sergey.testtinkoff.customerservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Customer service implementation.
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    private static final String CANT_FIND_CUSTOMER = "Can't find customer in database";

    private CustomerRepository customerRepository;
    private EntityToDtoMapper mapper;

    @Autowired
    CustomerServiceImpl(CustomerRepository customerRepository, EntityToDtoMapper mapper) {
        this.customerRepository = customerRepository;
        this.mapper = mapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(CustomerDto customerDto) {
        Customer customer = mapper.dtoToEntity(customerDto);
        customerRepository.save(customer);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(long customerId) {
        customerRepository.deleteById(customerId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(CustomerDto customerDto) {
        Customer customer = mapper.dtoToEntity(customerDto);
        Customer customerDB = customerRepository
                .findById(customerDto.getId())
                .orElseThrow(() -> new ApplicationRuntimeException(CANT_FIND_CUSTOMER));
        Customer updatedCustomer = updateCustomer(customerDB, customer);
        customerRepository.save(updatedCustomer);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CustomerDto getById(long customerId) {
        Customer customer = customerRepository
                .findById(customerId)
                .orElseThrow(() -> new ApplicationRuntimeException(CANT_FIND_CUSTOMER));
        return mapper.entityToDto(customer);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CustomerDto> getAll() {
        Iterable<Customer> allIter = customerRepository.findAll();
        ArrayList<CustomerDto> allCustomerDto = new ArrayList<>();
        for (Customer customer : allIter) {
            allCustomerDto.add(mapper.entityToDto(customer));
        }
        return allCustomerDto;
    }

    private Customer updateCustomer(Customer directionCustomer, Customer sourceCustomer) {
        directionCustomer.setFirstName(sourceCustomer.getFirstName());
        directionCustomer.setLastName(sourceCustomer.getLastName());
        directionCustomer.setAge(sourceCustomer.getAge());
        return directionCustomer;
    }
}
