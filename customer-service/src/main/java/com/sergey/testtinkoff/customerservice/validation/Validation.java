package com.sergey.testtinkoff.customerservice.validation;

import com.sergey.testtinkoff.customerservice.dto.CustomerDto;
import com.sergey.testtinkoff.customerservice.exception.ApplicationRuntimeException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Util class for validation.
 */
public class Validation {
    private static final String USER_ID_CANT_BE_NEGATIVE = "User id cannot be negative";
    private static final String USER_FIRST_NAME_2_20 = "Name can't be less than 2 or more than 20 letters";
    private static final String USER_FIRST_NAME_REGEX = "^[\\w+-]{2,20}$";
    private static final String USER_FIRST_NAME_NOT_NULL = "Name can't be null";
    private static final String USER_LAST_NAME_2_35 = "Last name can't be less than 2 or more than 35 letters";
    private static final String USER_LAST_NAME_REGEX = "^[\\w+-]{2,35}$";
    private static final String USER_LAST_NAME_NOT_NULL = "Last name can't be null";
    private static final String USER_AGE_NOT_VALID = "Age doesn't valid";

    /**
     * Validate that object doesn't null.
     *
     * @param object  object
     * @param message error message
     */
    public static void assertNotNull(Object object, String message) {
        if (object == null) {
            throw new ApplicationRuntimeException(message);
        }
    }

    /**
     * Validate customer DTO.
     *
     * @param customerDto customer DTO
     */
    public static void validateCustomerDto(CustomerDto customerDto) {
        validateCustomerId(customerDto.getId());
        assertNotNull(customerDto.getFirstName(), USER_FIRST_NAME_NOT_NULL);
        assertNotNull(customerDto.getLastName(), USER_LAST_NAME_NOT_NULL);
        validateRegex(customerDto.getFirstName(), USER_FIRST_NAME_REGEX, USER_FIRST_NAME_2_20);
        validateRegex(customerDto.getFirstName(), USER_LAST_NAME_REGEX, USER_LAST_NAME_2_35);
        validateCustomerAge(customerDto.getAge(), USER_AGE_NOT_VALID);
    }

    /**
     * Validate customer id.
     *
     * @param customerId customer id
     */
    public static void validateCustomerId(long customerId) {
        assertNotNegative(customerId, USER_ID_CANT_BE_NEGATIVE);
    }

    /**
     * Validate that customer id doesn't negative or 0.
     *
     * @param customerId customer id
     * @param message    error message
     */
    public static void assertNotNegative(long customerId, String message) {
        if (customerId < 0) {
            throw new ApplicationRuntimeException(message);
        }
    }

    /**
     * Validate customer age.
     *
     * @param age     customer age
     * @param message error message
     */
    public static void validateCustomerAge(int age, String message) {
        if (age < 0 || age >= 120) {
            throw new ApplicationRuntimeException(message);
        }
    }

    /**
     * Validates that provided string matches the specified regular expression.
     *
     * @param value to validate
     * @param regex to match
     */
    public static void validateRegex(String value, String regex, String message) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        assertTrue(matcher.matches(), message);
    }

    /**
     * Throws ApplicationRuntimeException if given condition is false.
     *
     * @param condition condition for assert
     * @param message   error massage
     */
    public static void assertTrue(boolean condition, String message) {
        if (!condition) {
            throw new ApplicationRuntimeException(message);
        }
    }

}
