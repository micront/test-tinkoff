package com.sergey.testtinkoff.customerservice.repository;

import com.sergey.testtinkoff.customerservice.domain.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for customers.
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
