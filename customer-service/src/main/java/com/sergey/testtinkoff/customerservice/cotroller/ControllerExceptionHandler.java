package com.sergey.testtinkoff.customerservice.cotroller;

import com.sergey.testtinkoff.customerservice.exception.ApplicationException;
import com.sergey.testtinkoff.customerservice.exception.ApplicationRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ValidationException;
import java.net.ConnectException;


/**
 * This @code{ControllerExceptionHandler} class handles exceptions.
 */
@ControllerAdvice
public class ControllerExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    /**
     * Handles exceptions while validation of view models.
     *
     * @param e - exception thrown
     * @return response entity with error code
     */
    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<String> handleValidationException(Exception e) {
        LOGGER.error("Validation exception, {}", e.getMessage(), e);

        return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * Handles exceptions while connect to other services.
     *
     * @param e - exception thrown
     * @return response entity with error code
     */
    @ExceptionHandler(ConnectException.class)
    public ResponseEntity<String> handleConnectException(Exception e) {
        LOGGER.error("Connect exception, {}", e.getMessage(), e);

        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Handles null pointer exceptions while user request.
     *
     * @param e - exception thrown
     * @return response entity with error code
     */
    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<String> handleNullPointerException(Exception e) {
        LOGGER.error("Null pointer exception, {}", e.getMessage(), e);

        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles our custom ApplicationRuntimeException.
     *
     * @param e - exception thrown
     * @return response entity with error code
     */
    @ExceptionHandler(ApplicationRuntimeException.class)
    public ResponseEntity<String> handleApplicationRuntimeException(Exception e) {
        LOGGER.error("Application runtime exception, {}", e.getMessage(), e);

        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Handles our custom ApplicationException.
     *
     * @param e - exception thrown
     * @return response entity with error code
     */
    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<String> handleApplicationException(Exception e) {
        LOGGER.error("Application exception, {}", e.getMessage(), e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
