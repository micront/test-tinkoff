package com.sergey.testtinkoff.customerservice.cotroller;

import com.sergey.testtinkoff.customerservice.dto.CustomerDto;
import com.sergey.testtinkoff.customerservice.service.CustomerService;
import com.sergey.testtinkoff.customerservice.validation.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Rest controller for customers.
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     * Create new customer.
     *
     * @param customerDto customer DTO
     * @return response entity
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Boolean> createCustomer(@RequestBody CustomerDto customerDto) {
        customerDto.setId(0);
        Validation.validateCustomerDto(customerDto);
        customerService.add(customerDto);
        return ResponseEntity.ok(true);
    }

    /**
     * Delete customer by id.
     *
     * @param customerId customer id
     * @return response entity
     */
    @RequestMapping(value = "/delete/{customerId}", method = RequestMethod.POST)
    public ResponseEntity<Boolean> deleteCustomer(@PathVariable("customerId") long customerId) {
        Validation.validateCustomerId(customerId);
        customerService.delete(customerId);
        return ResponseEntity.ok(true);
    }

    /**
     * Update customer.
     *
     * @param customerDto
     * @return response entity
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity<Boolean> updateCustomer(@RequestBody CustomerDto customerDto) {
        Validation.validateCustomerDto(customerDto);
        customerService.update(customerDto);
        return ResponseEntity.ok(true);
    }

    /**
     * Get customer by id.
     *
     * @param customerId customer id
     * @return customer DTO
     */
    @RequestMapping(value = "/get/{customerId}", method = RequestMethod.GET)
    public CustomerDto getCustomerById(@PathVariable("customerId") long customerId) {
        Validation.validateCustomerId(customerId);
        return customerService.getById(customerId);
    }

    /**
     * Get all customers.
     *
     * @return found customers
     */
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public List<CustomerDto> getAllCustomer() {
        return customerService.getAll();
    }
}
