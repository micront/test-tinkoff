package com.sergey.testtinkoff.customerservice.service;

import com.sergey.testtinkoff.customerservice.dto.CustomerDto;

import java.util.List;

/**
 * Service to manage customers.
 */
public interface CustomerService {
    /**
     * Add new customer.
     *
     * @param customerDto customer DTO
     */
    void add(CustomerDto customerDto);

    /**
     * Delete customer.
     *
     * @param customerId customer id
     */
    void delete(long customerId);

    /**
     * Update customer.
     *
     * @param customerDto customer DTO
     */
    void update(CustomerDto customerDto);

    /**
     * Get customer by id.
     *
     * @param customerId customer id
     * @return customer DTO
     */
    CustomerDto getById(long customerId);

    /**
     * Get all customers.
     *
     * @return list customers dto
     */
    List<CustomerDto> getAll();
}

