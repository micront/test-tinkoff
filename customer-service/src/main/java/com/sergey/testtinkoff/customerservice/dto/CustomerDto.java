package com.sergey.testtinkoff.customerservice.dto;

/**
 * DTO for customer entity.
 */
public class CustomerDto {
    private long id;
    private String firstName;
    private String lastName;
    private int age;

    public CustomerDto() {
    }

    public CustomerDto(long id, String firstName, String lastName, int age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    /**
     * Getter for id.
     *
     * @return id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter for id.
     *
     * @param id id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Getter for first name.
     *
     * @return first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter for first name.
     *
     * @param firstName first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter for last name.
     *
     * @return last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter for last name.
     *
     * @param lastName last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter for age.
     *
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * Setter for age.
     *
     * @param age age
     */
    public void setAge(int age) {
        this.age = age;
    }
}
