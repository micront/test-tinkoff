package com.sergey.testtinkoff.customerservice.mapper;

import com.sergey.testtinkoff.customerservice.domain.Customer;
import com.sergey.testtinkoff.customerservice.dto.CustomerDto;
import org.springframework.stereotype.Component;

/**
 * Class for mapping DTO to entity and back.
 */
@Component
public class EntityToDtoMapper {

    /**
     * Mapping entity to DTO.
     *
     * @param customer customer
     * @return customer DTO
     */
    public CustomerDto entityToDto(Customer customer) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(customer.getId());
        customerDto.setFirstName(customer.getFirstName());
        customerDto.setLastName(customer.getLastName());
        customerDto.setAge(customer.getAge());
        return customerDto;
    }

    /**
     * Mapping DTO to entity.
     *
     * @param customerDto customer DTO
     * @return customer
     */
    public Customer dtoToEntity(CustomerDto customerDto) {
        Customer customer = new Customer();
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setAge(customerDto.getAge());
        return customer;
    }
}
